-- Substitute Password --
-------------------------

-- Description --
-----------------
* This module allows administrators to set a ‘substitute password’ to all
individual user accounts which may be in any user Role.

* Website Administrator or developers can then be able to access the
user accounts (any role they may be in) and check what the functionalities
and the permissions are for that particular user/s.

* This module is used for the website developers and administrators to easily
set the substitute password and access the user account/s.

* Admin can still access the original password, and also the alternate
passwords.

* Admin will set this substitute password temporarily and then he can recall
the old password.

* This module has a backup of the old password. So old password doesn't get
removed and hence that is also safe.

* This is not to be confused with the ‘password reset’ functionality.

-- Dependencies --
------------------
Drupal 7 latest version.

-- INSTALLATION --
------------------
1. Download and install the Php finder module as normal.

2. Install modules on "sites/all/modules/contrib/" folder.

-- Configuration --
------------------
* Configure user permissions in Administration » People » Substitute Password.
