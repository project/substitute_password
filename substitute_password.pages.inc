<?php

/**
 * @file
 * Page callback: Password assign and rollback.
 */

/**
 * Page callback for for substitute password creation.
 */
function substitute_password_set_new_password_form($form, &$form_state, $uid) {

  $user = user_load($uid);
  $username = $user->name;

  // Only show name field on registration form or user can change own username.
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username:'),
    '#maxlength' => SUBSTITUTE_PASSWORD_NAME_MAX_LENGTH,
    '#default_value' => $username,
    '#attributes' => array('disabled' => 'disabled'),
  );

  $form['user_id'] = array(
    '#type' => 'hidden',
    '#size' => 10,
    '#value' => $user->uid,
  );

  $form['sub_password'] = array(
    '#type' => 'password_confirm',
    '#required' => TRUE,
    '#size' => 25,
    '#description' => t('To change the current user password, enter the new password in both fields.'),
    '#attributes' => array('class' => array('password-confirm')),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Page callback for for rollback old password.
 */
function substitute_password_set_new_password_form_submit($form, &$form_state) {

  require './includes/password.inc';
  $uid = $form_state['values']['user_id'];
  if ($uid) {
    // Get user information using UID.
    $user = user_load($uid);
    $user_id = $user->uid;
    $username = $user->name;
    $old_pass = $user->pass;
    $created_on = $user->created;
  }

  // Check password already exists or not.
  $check_password_assign = substitute_password_get_default_password($uid);
  if ($check_password_assign != '0') {
    drupal_set_message(t('Substitute password already created to @username', array('@username' => $username)));
    drupal_goto('admin/people/substitute-password');
  }

  $sub_password = $form_state['values']['sub_password'];
  $rehashed_password = user_hash_password($sub_password);

  // Maintain old password and new password into user_substitute_password table.
  $add_new_password = db_insert('user_substitute_password');
  $add_new_password->fields(
    array(
      'uid' => $user_id,
      'username' => $username,
      'old_pass' => $old_pass,
      'new_pass' => $rehashed_password,
      'created' => $created_on,
    )
    );
  $add_new_password->execute();

  // Update new password to 'users' table.
  $user->pass = $rehashed_password;
  user_save($user);

  drupal_set_message(t('Substitute password successfully created to @username', array('@username' => $username)));
  drupal_goto('admin/people/substitute-password');
}

/**
 * Page callback for for rollback old password.
 */
function substitute_password_rollback_old_password($uid) {

  if ($uid) {
    $user = user_load($uid);
    $user_id = $user->uid;
    $username = $user->name;
    // Get default password.
    $old_password = substitute_password_get_default_password($user_id);
    if ($old_password != '0') {
      // Update default password.
      $user->pass = $old_password;
      user_save($user);
      // Remove existing entry.
      substitute_password_remove_password_entry($user_id);
      drupal_set_message(t('Default password rollback successfully for @username', array('@username' => $username)));
      drupal_goto('admin/people/substitute-password');
    }
    else {
      drupal_set_message(t('Invalid User @username', array('@username' => $username)));
      drupal_goto('admin/people/substitute-password');
    }
  }
}

/**
 * Page callback: Generates the appropriate user administration form.
 */
function substitute_password($callback_arg = '') {
  $build['substitute_password_filter_form'] = drupal_get_form('substitute_password_filter_form');
  $build['substitute_password_admin_account'] = drupal_get_form('substitute_password_admin_account');
  return $build;
}

/**
 * Form builder; Return form for substitute password administration filters.
 *
 * @ingroup forms
 * @see substitute_password_filter_form_submit()
 */
function substitute_password_filter_form() {

  $session = isset($_SESSION['user_overview_filter']) ? $_SESSION['user_overview_filter'] : array();
  $filters = user_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only users where'),
    '#theme' => 'exposed_filters__user',
  );
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    if ($type == 'permission') {
      // Merge arrays of module permissions into one.
      // Slice past the first element '[any]' whose value is not an array.
      $options = call_user_func_array('array_merge', array_slice($filters[$type]['options'], 1));
      $value = $options[$value];
    }
    else {
      $value = $filters[$type]['options'][$value];
    }
    $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
    if ($i++) {
      $form['filters']['current'][] = array('#markup' => t('and where %property is %value', $t_args));
    }
    else {
      $form['filters']['current'][] = array('#markup' => t('%property is %value', $t_args));
    }
  }

  $form['filters']['status'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('clearfix')),
    '#prefix' => ($i ? '<div class="additional-filters">' . t('and where') . '</div>' : ''),
  );
  $form['filters']['status']['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('filters')),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status']['filters'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
      '#title' => $filter['title'],
      '#default_value' => '[any]',
    );
  }

  $form['filters']['status']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['status']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => (count($session) ? t('Refine') : t('Filter')),
  );
  if (count($session)) {
    $form['filters']['status']['actions']['undo'] = array(
      '#type' => 'submit',
      '#value' => t('Undo'),
    );
    $form['filters']['status']['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
  }

  drupal_add_library('system', 'drupal.form');

  return $form;
}

/**
 * Process result from substitute password administration filter form.
 */
function substitute_password_filter_form_submit($form, &$form_state) {

  $op = $form_state['values']['op'];
  $filters = user_filters();
  switch ($op) {
    case t('Filter'):
    case t('Refine'):
      // Apply every filter that has a choice selected other than 'any'.
      foreach ($filters as $filter => $options) {
        if (isset($form_state['values'][$filter]) && $form_state['values'][$filter] != '[any]') {
          // Merge an array of arrays into one if necessary.
          $options = ($filter == 'permission') ? form_options_flatten($filters[$filter]['options']) : $filters[$filter]['options'];
          // Only accept valid selections offered on dropdown block bad input.
          if (isset($options[$form_state['values'][$filter]])) {
            $_SESSION['user_overview_filter'][] = array($filter, $form_state['values'][$filter]);
          }
        }
      }
      break;

    case t('Undo'):
      array_pop($_SESSION['user_overview_filter']);
      break;

    case t('Reset'):
      $_SESSION['user_overview_filter'] = array();
      break;

    case t('Update'):
      return;
  }

  $form_state['redirect'] = 'admin/people/substitute-password';
}

/**
 * Form builder; List out users.
 */
function substitute_password_admin_account() {

  $header = array(
    'username' => array('data' => t('Username'), 'field' => 'u.name'),
    'status' => array('data' => t('Status'), 'field' => 'u.status'),
    'roles' => array('data' => t('Roles')),
    'member_for' => array(
      'data' => t('Member for'),
      'field' => 'u.created',
      'sort' => 'desc',
    ),
    'access' => array('data' => t('Last access'), 'field' => 'u.access'),
    'operations' => array('data' => t('Operations')),
  );

  $query = db_select('users', 'u');
  $query->condition('u.uid', 0, '<>');
  user_build_filter_query($query);

  $count_query = clone $query;
  $count_query->addExpression('COUNT(u.uid)');

  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
    ->fields('u', array('uid', 'name', 'status', 'created', 'access'))
    ->limit(50)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  $status = array(t('blocked'), t('active'));
  $roles = array_map('check_plain', user_roles(TRUE));
  foreach ($result as $account) {
    $users_roles = array();
    $roles_result = db_query('SELECT rid FROM {users_roles} WHERE uid = :uid', array(':uid' => $account->uid));
    foreach ($roles_result as $user_role) {
      $users_roles[] = $roles[$user_role->rid];
    }
    asort($users_roles);

    // Check password already assigned or not.
    $check_password_assign = substitute_password_get_default_password($account->uid);
    if ($check_password_assign != '0') {
      $op_title = t('Rollback Password');
      $op_herf = 'admin/people/rollback/' . $account->uid;
    }
    else {
      $op_title = t('Set Substitute Password');
      $op_herf = 'admin/people/assign/' . $account->uid;
    }

    $options[$account->uid] = array(
      'username' => theme('username', array('account' => $account)),
      'status' => $status[$account->status],
      'roles' => theme('item_list', array('items' => $users_roles)),
      'member_for' => format_interval(REQUEST_TIME - $account->created),
      'access' => $account->access ? t('@time ago', array('@time' => format_interval(REQUEST_TIME - $account->access))) : t('never'),
      'operations' => array(
        'data' =>
          array(
            '#type' => 'link',
            '#title' => $op_title,
            '#href' => $op_herf,
          ),
      ),
    );
  }

  $form['accounts'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No people available.'),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}

/**
 * Get default password.
 */
function substitute_password_get_default_password($uid, $field_name = 'old_pass') {

  $get_password = db_select('user_substitute_password', 'udp');
  $get_password->fields('udp', array($field_name));
  $get_password->condition('udp.uid', $uid);
  $results = $get_password->execute()->fetchCol(0);
  return !empty($results) ? $results[0] : '0';
}

/**
 * Remove existing password entry.
 */
function substitute_password_remove_password_entry($uid) {

  $remove_password_entry = db_delete('user_substitute_password');
  $remove_password_entry->condition('uid', $uid);
  $remove_password_entry->execute();
}
